﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using xRhodium.App_Start;
using xRhodium.Components;

namespace xRhodium.Controllers
{
    public class EnController : BaseController
    {
        public ActionResult Index(string r)
        {
            var viewModel = ViewModel<HomeViewModel>();
          //  // viewModel.Referer = r;
            viewModel.TimeStamp = GetTimestamp(DateTime.UtcNow);

            return View("Index", "_LayoutEmpty", viewModel);
        }

        public ActionResult RoadMap(string r)
        {
            var viewModel = ViewModel<HomeViewModel>();
          //  // viewModel.Referer = r;
            viewModel.TimeStamp = GetTimestamp(DateTime.UtcNow);

            return View("~/Views/En/RoadMap.cshtml", "_LayoutEmpty", viewModel);
        }

        public ActionResult CryptoTrinity(string r)
        {
            var viewModel = ViewModel<HomeViewModel>();
          //  // viewModel.Referer = r;
            viewModel.TimeStamp = GetTimestamp(DateTime.UtcNow);

            return View("CryptoTrinity", "_LayoutEmpty", viewModel);
        }

        public ActionResult StrongHandsAirdrop(string r)
        {
            var viewModel = ViewModel<HomeViewModel>();
          //  // viewModel.Referer = r;
            viewModel.TimeStamp = GetTimestamp(DateTime.UtcNow);

            return View("~/Views/En/StrongHandsAirdrop.cshtml", "_LayoutEmpty", viewModel);
        }

        public ActionResult FreeMarket(string r)
        {
            var viewModel = ViewModel<HomeViewModel>();
          //  // viewModel.Referer = r;
            viewModel.TimeStamp = GetTimestamp(DateTime.UtcNow);

            return View("FreeMarket", "_LayoutEmpty", viewModel);
        }

        public ActionResult AmbassadorProgram(string r)
        {
            var viewModel = ViewModel<HomeViewModel>();
          //  // viewModel.Referer = r;
            viewModel.TimeStamp = GetTimestamp(DateTime.UtcNow);

            return View("AmbassadorProgram", "_LayoutEmpty", viewModel);
        }

        public ActionResult SharingEssentials(string r)
        {
            var viewModel = ViewModel<HomeViewModel>();
            //  // viewModel.Referer = r;
            viewModel.TimeStamp = GetTimestamp(DateTime.UtcNow);

            return View("SharingEssentials", "_LayoutEmpty", viewModel);

        }

        public ActionResult Community(string r)
        {
            var viewModel = ViewModel<HomeViewModel>();
            // viewModel.Referer = r;
            viewModel.TimeStamp = GetTimestamp(DateTime.UtcNow);

            return View("Community", "_LayoutEmpty", viewModel);
        }

        public static string GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }
    }

    public class HomeViewModel
    {
        public string Referer { get; set; }
        public string TimeStamp { get; set; }
    }
}