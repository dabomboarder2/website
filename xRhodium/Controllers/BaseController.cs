﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.WebPages;
using xRhodium.Components;

namespace xRhodium.Controllers
{
    public abstract class BaseController : Controller
    {
        public const string LOGINERROR = "LoginError";
        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            HttpContext.SetOverriddenBrowser(BrowserOverride.Desktop);
            string cultureName = null;

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null) {
                cultureName = cultureCookie.Value;
            }
            else
            {
                cultureName = 
                    Request.UserLanguages != null && Request.UserLanguages.Length > 0 ? Request.UserLanguages[0] : null;
            }

            return base.BeginExecuteCore(callback, state);
        }

        protected TViewModel ViewModel<TViewModel>()
            where TViewModel : new()
        {
            var viewModel = new TViewModel();
            return viewModel;
        }
    }

    public class CrudViewModel
    {
        public string Title { get; set; }
        public bool ShowParralax { get; set; }
    }

    public class CrudViewModel<TEntity> : CrudViewModel where TEntity : new()
    {
        public TEntity Entity { get; set; }
        public IEnumerable<TEntity> Entities { get; set; }
        public string EntityTypeName
        {
            get { return typeof(TEntity).Name; }
        }

        public CrudViewModel()
        {
            Entity = new TEntity();
        }
    }

    public static class MyHelpers
    {
        public static string MyAction(this UrlHelper url, string actionName)
        {
            return MyAction(url, actionName, null);
        }

        public static string MyAction(this UrlHelper url, string actionName, object routeValues)
        {
            var currentController = View.CurrentPage.ViewContext.Controller.ValueProvider.GetValue("controller").RawValue.ToString();

            if (currentController.Length == 4)
            {
                currentController = currentController.Substring(0, 2) + "-" + currentController.Substring(2, 2);
            }

            return url.Action(actionName, currentController, routeValues);
        }
    }

    public static class View
    {

        public static IView Current
        {
            get { return WebPageContext.Current.Page as IView; }
        }

        public static WebViewPage CurrentPage
        {
            get { return WebPageContext.Current.Page as WebViewPage; }
        }

        public static HtmlHelper<object> Html
        {
            get { return CurrentPage.Html; }
        }

        public static HttpRequestBase Request
        {
            get { return CurrentPage.Request; }
        }

        public static UrlHelper Url
        {
            get { return CurrentPage.Url; }
        }
    }
}