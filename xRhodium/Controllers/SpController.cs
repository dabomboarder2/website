﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace xRhodium.Controllers
{
    public class SpController : BaseController
    {
        public ActionResult Index(string r)
        {
            return RedirectToAction("Index", "Es", new { r });
        }

        public ActionResult RoadMap(string r)
        {
            return RedirectToAction("RoadMap", "Es", new { r });
        }

        public ActionResult CryptoTrinity(string r)
        {
            return RedirectToAction("CryptoTrinity", "Es", new { r });
        }

        public ActionResult StrongHandsAirdrop(string r)
        {
            return RedirectToAction("StrongHandsAirdrop", "Es", new { r });
        }

        public ActionResult FreeMarket(string r)
        {
            return RedirectToAction("FreeMarket", "Es", new { r });
        }

        public ActionResult AmbassadorProgram(string r)
        {
            return RedirectToAction("AmbassadorProgram", "Es", new { r });
        }

        public ActionResult Community(string r)
        {
            var viewModel = ViewModel<HomeViewModel>();
            // viewModel.Referer = r;
            viewModel.TimeStamp = GetTimestamp(DateTime.UtcNow);

            return View("~/Views/Es/Community.cshtml", "_LayoutEmpty", viewModel);
        }

        public ActionResult SharingEssentials(string r)
        {
            var viewModel = ViewModel<HomeViewModel>();
            //  // viewModel.Referer = r;
            viewModel.TimeStamp = GetTimestamp(DateTime.UtcNow);

            return View("~/Views/Es/SharingEssentials.cshtml", "_LayoutEmpty", viewModel);

        }

        public static string GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }
    }
}