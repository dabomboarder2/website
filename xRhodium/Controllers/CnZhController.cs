﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace xRhodium.Controllers
{
    public class CnZhController : BaseController
    {
        // GET: RuRu
        public ActionResult Index(string r)
        {
            var viewModel = ViewModel<HomeViewModel>();
            // viewModel.Referer = r;
            viewModel.TimeStamp = GetTimestamp(DateTime.UtcNow);

            return View("~/Views/Cn/Index.cshtml", "_LayoutEmpty", viewModel);
        }

        public ActionResult RoadMap(string r)
        {
            var viewModel = ViewModel<HomeViewModel>();
            // viewModel.Referer = r;
            viewModel.TimeStamp = GetTimestamp(DateTime.UtcNow);

            return View("~/Views/En/RoadMap.cshtml", "_LayoutEmpty", viewModel);
        }

        public ActionResult CryptoTrinity(string r)
        {
            var viewModel = ViewModel<HomeViewModel>();
            // viewModel.Referer = r;
            viewModel.TimeStamp = GetTimestamp(DateTime.UtcNow);

            return View("~/Views/Cn/CryptoTrinity.cshtml", "_LayoutEmpty", viewModel);
        }

        public ActionResult StrongHandsAirdrop(string r)
        {
            var viewModel = ViewModel<HomeViewModel>();
            // viewModel.Referer = r;
            viewModel.TimeStamp = GetTimestamp(DateTime.UtcNow);

            return View("~/Views/En/StrongHandsAirdrop.cshtml", "_LayoutEmpty", viewModel);
        }

        public ActionResult FreeMarket(string r)
        {
            var viewModel = ViewModel<HomeViewModel>();
            // viewModel.Referer = r;
            viewModel.TimeStamp = GetTimestamp(DateTime.UtcNow);

            return View("~/Views/Cn/FreeMarket.cshtml", "_LayoutEmpty", viewModel);
        }

        public ActionResult AmbassadorProgram(string r)
        {
            var viewModel = ViewModel<HomeViewModel>();
            // viewModel.Referer = r;
            viewModel.TimeStamp = GetTimestamp(DateTime.UtcNow);

            return View("~/Views/Cn/AmbassadorProgram.cshtml", "_LayoutEmpty", viewModel);
        }

        public ActionResult Community(string r)
        {
            var viewModel = ViewModel<HomeViewModel>();
            // viewModel.Referer = r;
            viewModel.TimeStamp = GetTimestamp(DateTime.UtcNow);

            return View("~/Views/Cn/Community.cshtml", "_LayoutEmpty", viewModel);
        }

        public ActionResult SharingEssentials(string r)
        {
            var viewModel = ViewModel<HomeViewModel>();
            //  // viewModel.Referer = r;
            viewModel.TimeStamp = GetTimestamp(DateTime.UtcNow);

            return View("~/Views/Cn/SharingEssentials.cshtml", "_LayoutEmpty", viewModel);

        }

        public static string GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }
    }
}